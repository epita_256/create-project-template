import inquirer from "inquirer";
import { prompts } from "#constants";

inquirer
  .prompt(prompts)
  .then((answers: any) => {
    if (answers.createAliases === "Yes") {
      inquirer
        .prompt([
          {
            name: "enterAliasPrefix",
            message: "Enter alias prefix",
          },
        ])
        .then((ans: any) => console.log("Answers:", { ...answers, ...ans }));
    } else console.log("Answers:", answers);
  })
  .catch((error: any) => {
    if (error.isTtyError) {
      console.log(error.isTtyError);
    } else {
      console.log(error);
    }
  });
